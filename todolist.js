/**
 * Задание 1 - Активная ссылка
 *
 * Задание 2 - Добавление и удаление элементов списка
 *
 * Задание 3 - Перетаскивание элемента
 */


let dragged;
let list = document.querySelector('.list');
let dragContainers = document.querySelectorAll('.list');
document.addEventListener("DOMContentLoaded", function(){

    // В цикле вызываем функцию для каждой ссылки в меню
    list.querySelectorAll('a').forEach(function(link){
        addLinkListener(link);
    });

    // Устанавливаем слушатель на кнопку добавить
    document.querySelector('.add-item').addEventListener('click', () => addNewListItem());

    // Устанавливаем слушатель на кнопку добавить
    document.querySelector('.remove-item').addEventListener('click', () => removeLastListItem());

    draggableCards().forEach(card => addDragListeners(card));

    dragContainers.forEach(dragContainer => {
        dragContainer.addEventListener('drop', (e) => {
            e.preventDefault();
            onDrop(e);
        });
        dragContainer.addEventListener('dragover', (e) => {
            e.preventDefault();
            dragOver(e);
        });
    })

});

function addDragListeners(card){
    card.addEventListener('dragstart', function(e){
        e.dataTransfer.setData("text/html", e.target.outerHTML);
        dragged = card;
    });
    card.addEventListener('dragend', function(e){
        e.preventDefault();
    });
    card.addEventListener('drag', function(e){
    });
}

// Устанавливаем слушателя на ссылку и описываем порядок переключения классов
function addLinkListener(link){
    link.addEventListener('click', function(e){
        e.preventDefault();
        removeActiveClass(this.closest('.list'));
        this.classList.toggle('active');
    });
}

// Удаляем класс "active" у всех ссылок
function removeActiveClass(currList){
    currList.querySelectorAll('a').forEach(item => item.classList.remove('active'));
}

// Копируем последний элемент списка
// Удаляем класс "active" и изменяем текстовое содержимое
// Устанавливаем слушателя на новую ссылку
function addNewListItem(){
    let newItem = list.lastElementChild.cloneNode(true);
    newItemLink = newItem.firstElementChild;
    newItemLink.classList.remove('active');
    newItemLink.textContent = 'Item ' + (list.children.length + 1);
    addLinkListener(newItemLink);
    addDragListeners(newItem);
    list.appendChild(newItem);
}

// Если элементы есть то удаляем
function removeLastListItem(){
    if(list.children.length > 1){
        list.lastElementChild.remove();
    }
}

function draggableCards(){
    return document.querySelectorAll('.draggable');
}

function dragOver(e){
    e.preventDefault();
}

function onDrop(e){
    e.preventDefault();
    let oldCard = dragged.parentNode.removeChild( dragged );
    e.target.appendChild(oldCard);
    addLinkListener(oldCard.firstElementChild);
    addDragListeners(oldCard);
}